import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Assignment3and4 extends Application {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		// TODO Auto-generated method stub

		// 1. Creating the Interface using Label and Textbox
		// to accept value
		Label locationLabel = new Label("From: ");
		
		ObservableList<String> items = FXCollections.observableArrayList("Brampton", "Cestar College");
		ComboBox<String> locationTextBox = new ComboBox<>(items);
		
		// Check Box
		CheckBox checkbox1 = new CheckBox("Extra Luggage?");
		CheckBox checkbox2 = new CheckBox("Pets");
		CheckBox checkbox3 = new CheckBox("Use 407 ETR?");
		CheckBox checkbox4 = new CheckBox("Add Tip?");

		
		// calculate button
		Button btn = new Button("Calculate");
		Label resultLabel = new Label("");
		
		
		btn.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				System.out.println("Button Pressed!");
				String location = locationTextBox.getValue();
				double total = 0;
				double base = 0;
				int distance = 0;
				if(!location.trim().isEmpty() && location.contentEquals("Brampton")) {
					base = 38;
					distance = 23;
				}else {
					base = 51;
					distance = 26;
				}
				
				total += base;
				
				if (checkbox1.isSelected()) {
					total += 10;
				}

				if (checkbox2.isSelected()) {
					total += 6;
				}
				if (checkbox3.isSelected()) {
					total += 0.25 * distance; 
				}
				
				//Tax
				total += total * 0.13;
				
				if (checkbox4.isSelected()) {
					total += 0.15 * total;
				}
				
				resultLabel.setText("The total fare is: $"+String.format("%.4g%n",total));
			}
		});

		// 2. Make a layout manager
		VBox root = new VBox();

		// 3. Add controls to the layout manager
		root.getChildren().add(locationLabel);
		root.getChildren().add(locationTextBox);
		root.getChildren().add(checkbox1);
		root.getChildren().add(checkbox2);
		root.getChildren().add(checkbox3);
		root.getChildren().add(checkbox4);
		root.getChildren().add(btn);
		root.getChildren().add(resultLabel);
		root.setPadding(new Insets(10,80,10,80));
		root.setSpacing(10);
		
		// 4. Add layout manager to scene
		// 5. Add scene to a stage
		primaryStage.setScene(new Scene(root, 450, 300));
		primaryStage.setTitle("Fare Calculator");
		// 6. Show the app
		primaryStage.show();
	}

}
